const express = require('express')
const { v4: makeUUID } = require('uuid')
const app = express()

require('dotenv').config()

const PORT = 8068 // of this webhook catcher

const VERIFIER_URL = process.env.API_URL
const VERIFIER_APIKEY = process.env.API_KEY
const PRESENTATION_URL = process.env.PRES_URL
let newPresentationId

app.use(express.json())

app.post('/', function (req, res) {
  const { state, goalCode, connectionId, status, presentationId } = req?.body?.data || {}
  const { type } = req?.body

  console.log({ newPresentationId })
  console.log(req.body)

  if (
    connectionId 
    && type === 'ConnectionUpdated' 
    && state === 'ConnectionResponseSent' 
    && goalCode === 'beta-galaxy-maps-vc'
  ) {

  console.log('===Connection successful!!!===!')

  fetch(`${VERIFIER_URL}/present-proof/presentations`, {
    method: 'POST',
    headers: {
      accept: 'application/json',
      'Content-Type': 'application/json',
      apiKey: VERIFIER_APIKEY
    },
    body: JSON.stringify({
      connectionId,
      options: {
        challenge: makeUUID(), // random seed prover has to sign to prevent replay attacks
        domain: 'www.galaxymaps.io'
      },
      proofs: [
        // TODO figure out how to manage proof Schemas
        // https://docs.atalaprism.io/agent-api/#tag/Present-Proof/operation/requestPresentation
        //
        // TODO figure out how / whether to use trustedIssuerDID
      ]
    })
  })
    .then(response => response.json())
    .then(result => {
      console.log('Presentation req sent', result)
      newPresentationId = result.presentationId
      res.sendStatus(200)
    })
    .catch(console.error)
  }

  if (
	  presentationId === newPresentationId
	  && type === 'PresentationUpdated' 
	  && status === 'PresentationVerified'
  ) {
    
	  console.log('===Presentation verified===')
      fetch(`${VERIFIER_URL}/present-proof/presentations/${presentationId}`, {
      method: 'PATCH',
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/json',
        apiKey: VERIFIER_APIKEY
      },
      body: JSON.stringify({
	      action: 'presentation-accept'
        })
      })
      .then(response => response.json())
      .then(result => {
        console.log('Presentation accepted', result)
        res.sendStatus(200)
      })
      .catch(console.error)
    }
  

	if (
    newPresentationId === presentationId
    && type === 'PresentationUpdated' 
    && status === 'PresentationAccepted'
  ) {
      console.log('===Presentation Accepted send something to GM===')
      fetch(`${PRESENTATION_URL}`, {
        method: 'GET',
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          data: {
            studentId: presentationId
          }
        })
      })
    }
})

app.listen(PORT)

// Where we got to:
//
// - can set up webhooks
// - the oob can be decoded with JSON.parse(Buffer.from(oob, 'base64').toString())
//    - this gets us the fromDID
//    - allows us to know subsequent messages associated with the code we actively pasted in
//    - can know it's ok to auto-reply to those

// TODO:
// - auto-reply to messages which we input an oob for << ssb-atala-prism
// - have this listen for credentials complete, and update firebase  << here

// Questions:
// - how do we know which oob was associated with which presentation?

// NOTE
// - currently set env with
//   - export API_KEY=abc
//   - export API_URL=http...
// - you have to set up the webhooks to point at this server! (see webhooks/post.js)
//
