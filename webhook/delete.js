require('dotenv').config()

const id = process.argv[2]

if (!id) throw new Error('id required')

const url = `${process.env.API_URL}/events/webhooks/${id}`
console.log('DELETE', url)

fetch(url, {
  method: 'DELETE',
  headers: {
    'Content-Type': 'application/json',
    apiKey: process.env.API_KEY
  }
})
  .then(res => {
	  console.log(res)
	  res.json()
  })
  .then(res => console.log('success', res))
  .catch(err => console.log('failure', err))
