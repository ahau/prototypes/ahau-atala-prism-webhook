require('dotenv').config

const address = process.argv[2]

if (!address) throw new Error('address required')

const url = `${process.env.API_URL}/events/webhooks`
const data = { url: address }
console.log('POST', url, data)

fetch(url, {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
    apiKey: process.env.API_KEY
  },
  body: JSON.stringify(data)
})
  .then(res => res.json())
  .then(res => console.log('success', res))
  .catch(err => console.log('failure', err))
