require('dotenv').config()
const url = `${process.env.API_URL}/events/webhooks`
console.log('GET', url)

fetch(url, {
  method: 'GET',
  headers: {
    'Content-Type': 'application/json',
    apiKey: process.env.API_KEY
  }
})
  .then(res => res.json())
  .then(res => console.log('success', res))
  .catch(err => console.log('failure', err))
